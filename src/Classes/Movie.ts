import { IMovie } from "../Intefaces/Imovie";

export class Movie implements IMovie{
    readonly Id: number;
    readonly Title: string;
    readonly Description: string;
    readonly Image: string;
    readonly Watched:Date;
    readonly Banner:string;
    constructor(id:number,title:string,description:string,image:string,banner:string,watched:Date){
        this.Id=id;
        this.Title=title;
        this.Description=description;
        this.Image=image;
        this.Banner=banner;
        this.Watched=watched;
    }
}