import { Movie } from "./Classes/Movie";
import { createMovieDom } from "./helpers/dom_helpers";
import { renderFavorites, renderMovies } from "./helpers/render_Helper";
export async function render(): Promise<void> {
    await renderMovies(1,'popular');
    await renderFavorites();
}
