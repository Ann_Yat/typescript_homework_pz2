import { Movie } from "../Classes/Movie";
import { getMovie, getMovies, movieType } from "./API_Helper";
import { createBanner, createMovieDom } from "./dom_helpers";
import { getFavoritesId } from "./storage_helper";

export async function renderMovies(page:number,type:movieType):Promise<void> {
    const moovies:Array<Movie> = await getMovies(page,type);
    renderPage(moovies,page);
    renderRandomMovie(moovies);
}

export async function renderFavorites():Promise<void> {
    const fav:Array<number> = getFavoritesId();
    const moovies:Array<Movie> = []
    fav.forEach(element => {
       moovies.push(getMovie(element)); 
    });
    renderRightSide(moovies);
}

export async function renderRandomMovie(moovies:Array<Movie>):Promise<void>  {
    const movie:Movie =  moovies[Math.floor(Math.random() * moovies.length)]
    renderBanner(movie);
}

export async function renderSearch(page:number,query:string):Promise<void> {
    if(query===''){
        alert("Hey, its empty string! I can`t find movie without title! ")
        return;
    }
    const moovies:Array<Movie> = await getMovies(page,'search',query);
    renderPage(moovies,page);
    renderRandomMovie(moovies);
}

function renderPage(moovies:Array<Movie>,page:number):void {
    const film_container = document.getElementById("film-container");
    if(film_container){
        if(page===1){ film_container.innerHTML ="" ;}
        moovies.forEach(movie => { 
           film_container.append(createMovieDom(movie));
        });
    }
}

function renderBanner(movie: Movie) {
    const random_movie = document.getElementById("random-movie");
    if(random_movie){
        random_movie.innerHTML="";
        random_movie.append(createBanner(movie));
    }
}
function renderRightSide(moovies:Array<Movie>):void {
    const favorite_movies = document.getElementById("favorite-movies");
    if(favorite_movies){
        favorite_movies.innerHTML ="" ;
        moovies.forEach(movie => { 
            favorite_movies.append(createMovieDom(movie,true));
        });
    }
}

