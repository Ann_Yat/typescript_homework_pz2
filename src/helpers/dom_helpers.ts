import { main } from "@popperjs/core";
import { Movie } from "../Classes/Movie";
import { renderFavorites } from "./render_Helper";
import { isFav, toggleFav } from "./storage_helper";

type SvgInHtml = HTMLElement & SVGElement;


 export function createMovieDom(movie:Movie,right?:boolean):HTMLElement{
    var mainpart:HTMLElement ;
         if(!right){
             mainpart =  createDom("div",["col-12","p-2","col-lg-3","col-md-4"]);
         }else{
             mainpart =  createDom("div",["col-12","p-2"]);
         }  
    const innerCard = createDom("div",["card","shadow-sm"])
    const image = createImage('https://image.tmdb.org/t/p/original/'+movie.Image,[]);
    innerCard.append(image);
    innerCard.append(createHeart(isFav(movie.Id),movie.Id));
    const body = createDom("div",["card-body"])
    const text = createDom("p",["card-text","truncate"])
        text.innerText = movie.Description;
    body.append(text);
    const date = createDom("div",["d-flex","justify-content-between","align-items-center"])
    const small = createDom("small",["text-muted"])
        small.innerText = movie.Watched.getFullYear()+'-'+movie.Watched.getMonth()+'-'+movie.Watched.getDate();
    date.append(small);    
    body.append(date);
    innerCard.append(body);
    mainpart.append(innerCard);
    return mainpart;
 }
 
 export function createBanner(movie:Movie):HTMLElement{
    const banner:HTMLElement = createDom("div",["row","py-lg-5"])
        banner.style.maxHeight="600px";
        banner.style.minHeight="400px";
        const background:string= `url('${movie.Banner}')`;
        banner.style.background=background;
        banner.style.backgroundSize="cover";
    const inner:HTMLElement = createDom("div",["col-lg-6","col-md-8","mx-auto"])
        inner.style.backgroundColor="#2525254f";
    const title:HTMLElement = createDom("h1",["fw-light","text-light"])
        title.id="random-movie-name";
        title.innerText=movie.Title;
    inner.append(title);
    const body:HTMLElement = createDom("p",["lead","text-white"])
        body.id="random-movie-description";
        body.style.paddingTop="auto"
        body.style.paddingBottom="auto"
        body.innerText = movie.Description;
    inner.append(body);
    banner.append(inner);    
    return banner;
 }

 function createHeart(isFav:boolean,id:number):SVGElement{
    const heart  = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        heart.setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns', 'http://www.w3.org/2000/svg');
        heart.setAttributeNS('http://www.w3.org/2000/svg', 'viewBox', '0 -2 38 42');
        heart.classList.add("bi");
        heart.classList.add("bi-heart-fill");
        heart.classList.add("position-absolute");
        heart.classList.add("p-2");
        heart.classList.add("heart");
        heart.classList.add(id.toString());
        if(isFav){
            heart.classList.add("heart_active"); 
        }
        heart.dataset['fav']=id.toString();
        heart.onclick = function(e:MouseEvent){
            const object = <HTMLElement> e.target;
            if(object.dataset['fav']){
                toggleFav(+object.dataset.fav);
                const objs:HTMLCollectionOf<Element> = document.getElementsByClassName(object.dataset.fav); 
                for(let i:number = 0;i<objs.length;i++){
                    const button = <HTMLInputElement>objs.item(i);
                    if(button.classList.contains("heart_active")){
                        button.classList.remove("heart_active");
                    }else{
                        button.classList.add("heart_active");
                    }
                }
            }
           
            renderFavorites();
        }
    const path = document.createElementNS("http://www.w3.org/2000/svg", "path");
        path.setAttributeNS(null, 'fill-rule', 'evenodd');
        path.dataset['fav']=id.toString();
        path.setAttributeNS(null, 'd', 'M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z');
    heart.append(path);
    return heart;
 }

 function createDom(tag:string,classes:Array<string>):HTMLElement{
     const element:HTMLElement = document.createElement(tag);
     classes.forEach(e => {
        element.classList.add(e);
     });
     return element;
 }
 function createImage(src:string,classes:Array<string>):HTMLImageElement{
     const image:HTMLImageElement =<HTMLImageElement> createDom("img",classes);
     image.src = src;
     return image; 
 }