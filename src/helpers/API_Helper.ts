import { Movie } from "../Classes/Movie";
import { API_KEY, API_MOVIE, API_POPULAR, API_SEARCH, API_TOP, API_UPCOMING, API_URL } from "../secret/codes";
import { movieParse } from "./parser_Helper";

export type movieType = 'popular'|'top_rated'|'upcoming'|'search'

export async function getMovies(page:number,form:movieType,zapros?:string):Promise<Array<Movie>>{
const movies = Array<Movie>();
const key:string = API_KEY;
const url:string= API_URL;
var api:string;
switch(form){
    case 'search':api=API_SEARCH;
    break;
    case 'top_rated':api = API_TOP;
    break;
    case 'upcoming':api = API_UPCOMING;
    break;
    case 'popular':
    default:api = API_POPULAR;
}
const xmlHttp = new XMLHttpRequest();
var query:string
if(form==='search'){
    query= url+api+'?api_key='+key+'&query='+zapros+'&page='+page;
}else{

    query= url+api+'?api_key='+key+'&page='+page;
}
xmlHttp.open( "GET", query, false ); 
xmlHttp.send( null );
const array = JSON.parse(xmlHttp.responseText).results;
if(array){
    array.forEach((e: any) => {
        movies.push(movieParse(e));
    });
}
return movies;
}

export function getMovie(id:number):Movie{
    const key:string = API_KEY;
    const url:string= API_URL;
    const api:string =API_MOVIE+id;
    const query:string= url+api+'?api_key='+key;
    const xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", query, false ); 
    xmlHttp.send( null );
    const mov = JSON.parse(xmlHttp.responseText);
    var moovie:Movie;
    if(mov){  
        moovie = (movieParse(mov));  
    }else{
        moovie=new Movie(0,'Cruella','Good moovie!','/6MKr3KgOLmzOP6MSuZERO41Lpkt.jpg','/rTh4K5uw9HypmpGslcKd4QfHl93.jpg',new Date());
    }
    return moovie;
}