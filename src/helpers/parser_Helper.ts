import { Movie } from "../Classes/Movie"

export function movieParse(raw_data: { id: number; original_title: string; overview: string; backdrop_path:string; poster_path: string; release_date: string ; }):Movie{
    if(!raw_data.backdrop_path){
        raw_data.backdrop_path='/6MKr3KgOLmzOP6MSuZERO41Lpkt.jpg';
    }
    if(!raw_data.poster_path){
        raw_data.poster_path='/rTh4K5uw9HypmpGslcKd4QfHl93.jpg';
    }
const moovie = new Movie(raw_data.id,raw_data.original_title,raw_data.overview,raw_data.poster_path,'https://image.tmdb.org/t/p/original/'+raw_data.backdrop_path,new Date(raw_data.release_date));
return moovie;
}