export function getFavoritesId():Array<number>{
    const storaged:string|null = localStorage.getItem("moovies08");
    if(storaged){
        const fav:Array<number> = JSON.parse(storaged);
        return fav;
    }
    else{
        localStorage.setItem("moovies08",JSON.stringify([]));
        return [];
    }
}

export function isFav(id:number):boolean{
    const fav:Array<number> = getFavoritesId();
    return fav.includes(id);
}

export function toggleFav(id:number):void{
    const fav:Array<number> = getFavoritesId();
    if(isFav(id)){
        fav.forEach((element,index)=>{
            if(element==id) fav.splice(index,1);
         });
    }else{
        fav.push(id);
    }
    localStorage.setItem("moovies08",JSON.stringify(fav));
}