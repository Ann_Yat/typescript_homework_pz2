export interface IMovie{
    readonly Id:number,
    readonly Title:string,
    readonly Description:string,
    readonly Image:string
    readonly Watched:Date
    readonly Banner:string
}