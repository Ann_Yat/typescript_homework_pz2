export const PORT=9090
export const API_KEY='fc527a16c2cc5440f032363b91d14e93'
export const API_URL='https://api.themoviedb.org/3'
export const API_POPULAR='/movie/popular'
export const API_UPCOMING='/movie/upcoming'
export const API_TOP='/movie/top_rated'
export const API_SEARCH='/search/movie'
export const API_MOVIE='/movie/'