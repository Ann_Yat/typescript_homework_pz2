import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './src/styles/styles.css';
import { render } from './src/index';
import { renderMovies, renderSearch } from './src/helpers/render_Helper';
import { movieType } from './src/helpers/API_Helper';

const upcoming = document.getElementById("upcoming");
if(upcoming){
    upcoming.onclick = function(){
        renderMovies(1,'upcoming');
        const load_more = document.getElementById("load-more");
        if(load_more){
            load_more.dataset.page='1';
        }
    }
}
const popular = document.getElementById("popular");
if(popular){
    popular.onclick = function(){
        renderMovies(1,'popular');
        const load_more = document.getElementById("load-more");
        if(load_more){
            load_more.dataset.page='1';
        }
    }
}
const top_rated = document.getElementById("top_rated");
if(top_rated){
    top_rated.onclick = function(){
        renderMovies(1,'top_rated');
        const load_more = document.getElementById("load-more");
        if(load_more){
            load_more.dataset.page='1';
        }
    }
}
const search = document.getElementById("submit");
if(search){
    search.onclick = function(){
        const input =<HTMLInputElement>  document.getElementById("search");
        renderSearch(1,input.value);
        const load_more = document.getElementById("load-more");
        if(load_more){
            load_more.dataset.page='1';
        }
        const buttons:HTMLCollectionOf<Element> = document.getElementsByClassName("btn-check");
            for(let i:number = 0;i<buttons.length;i++){
                const button = <HTMLInputElement>buttons.item(i);
               button.checked=false;
            }
    }
}
const load_more = document.getElementById("load-more");
if(load_more){
    load_more.onclick = function(e:MouseEvent){
        const object:HTMLElement = (<HTMLElement>e.target);
        const str:string|undefined = (object.dataset.page);
        if(str){
            const page = +str+1;
            const buttons:HTMLCollectionOf<Element> = document.getElementsByClassName("btn-check");
            var isSearch:boolean = true;
            for(let i:number = 0;i<buttons.length;i++){
                const button = <HTMLInputElement>buttons.item(i);
                if(button.checked){
                    if(button){
                        if(button.id){
                            renderMovies(page,<movieType>button.id);
                            object.dataset.page = page.toString();
                        }
                    }
                    isSearch=false;
                    break;
                }
            }
            if(isSearch){
                const input =<HTMLInputElement>  document.getElementById("search");
                renderSearch(page,input.value);
            }
        }
    }
}



render();
